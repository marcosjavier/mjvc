<?php

class Index extends MY_Model {

	public function getMenu(){
		$sql = "SELECT m.nombre,m.icono,m.referencia,m.idref
				FROM menu m 
				WHERE inView = 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getComisiones(){
		$sql = "SELECT c.nombre,c.icono,c.descripcion
				FROM comision c";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getFechas(){
		$sql = "SELECT f.imagen,f.titulo,f.descripcion,f.descripcionAdicional,f.pathImgReduced,f.pathImgOriginal
				FROM fechas f
				WHERE inView = 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	} 

	public function getImagenesPrimerSetJornadas(){
		$sql = "SELECT j.nombre,j.descripcion,j.pathImgReduced,j.pathImgOriginal
				FROM jornadas j
				WHERE imgInView = 1
				LIMIT 3";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getImagenesSegundoSetJornadas(){
		$sql = "SELECT j.nombre,j.descripcion,j.pathImgReduced,j.pathImgOriginal
				FROM jornadas j
				WHERE imgInView = 1
				LIMIT 3,3";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getImagenesTercerSetJornadas(){
		$sql = "SELECT j.nombre,j.descripcion,j.pathImgReduced,j.pathImgOriginal
				FROM jornadas j
				WHERE imgInView = 1
				LIMIT 6,3";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getTestimonios(){
		$sql = "SELECT t.jornadista,t.jornada,t.testimonio
				FROM testimonio t
				WHERE inView = 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function setInscripcionConvivencia($datos){
		$sql = "INSERT INTO convivencia(nombres,apellido,correo,telefono) VALUES(?,?,?,?)";
		$query = $this->db->query($sql,array($datos['nombres'],$datos['apellido'],$datos['email'],$datos['telefono']));
	}

	public function procesarFichaMedica($datos){
		$sql = "INSERT INTO fichamedica(nombreCompleto,grupoSanguineo,telefono,email,diabetes,alergias,nombreAlergia,problemaCoagulacion,celiaco,asma,cardiopatia,hipertension,otraEnfermedad,medicamentoPorEnfermedad,medicamentoASuministrar,dosisMedicamentoASuministrar,nombreMedicamentoOcasional,nombreMedicamentoProhibido) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($sql,array($datos['nombreCompleto'],$datos['grupoSanguineo'],$datos['telefono'],$datos['email'],$datos['diabetes'],$datos['alergias'],$datos['nombreAlergia'],$datos['problemaCoagulacion'],$datos['celiaco'],$datos['asma'],$datos['cardiopatia'],$datos['hipertension'],$datos['otraEnfermedad'],$datos['medicamentoPorEnfermedad'],$datos['medicamentoASuministrar'],$datos['dosisMedicamentoASuministrar'],$datos['nombreMedicamentoOcasional'],$datos['nombreMedicamentoProhibido']));
		if(!empty($query))
			return true;
		else
			return false; 
	}

	public function procesarFichaPadrino($datos){
		$sql = "INSERT INTO fichapadrino(nombreCompleto,telefono,domicilio,celular,email,localidad,edad,jornada,ahijado,conocimientoAhijado,relacionConJornadas,motivoJornada,ambienteSocial,defectosYVirtudes,vidaCristiana,hechosSignificativos) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($sql,array($datos['nombreCompleto'],$datos['telefono'],$datos['domicilio'],$datos['celular'],$datos['email'],$datos['localidad'],$datos['edad'],$datos['jornada'],$datos['ahijado'],$datos['conocimientoDelAhijado'],$datos['relacionConJornadas'],$datos['motivoJornada'],$datos['ambienteSocial'],$datos['defectosYVirtudes'],$datos['vidaCristiana'],$datos['hechosSignificativos']));
		if(!empty($query))
			return true;
		else
			return false; 
	}

	public function procesarFichaAhijado($datos){
		$sql = "INSERT INTO fichaahijado(nombreCompleto,fechaNacimiento,domicilio,localidad,email,telefono,celular,estudiosCursados,titulo,trabajo,ubicacion,estadoCivil,hijos,cantidad,edad,nombreCompletoPadrino,hechosSignificativos,ideaDios,sacramentos,sacerdotes,trabajoApostolico,ambienteSocial,virtudesYDefectos,motivoJornada) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($sql,array($datos['nombreCompleto'],$datos['fechaNacimiento'],$datos['domicilio'],$datos['localidad'],$datos['email'],$datos['telefono'],$datos['celular'],$datos['estudios'],$datos['titulo'],$datos['trabajo'],$datos['ubicacion'],$datos['estadoCivil'],$datos['hijos'],$datos['cantidadHijos'],$datos['edad'],$datos['nombreCompletoPadrino'],$datos['hechosSignificativos'],$datos['ideaDeDios'],$datos['sacramentos'],$datos['relacionConSacerdotes'],$datos['trabajosApostolicos'],$datos['ambienteSocial'],$datos['defectosYVirtudes'],$datos['motivoJornada']));
		if(!empty($query))
			return true;
		else
			return false; 
	}

	public function getFechasDisponiblesRosario(){
		$sql = "SELECT DISTINCT(r.fechaHora)
				FROM rosario r
				WHERE r.disponible like '1'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getFechasTomadasRosario(){
		$sql = "SELECT DISTINCT(r.fechaHora)
				FROM rosario r
				WHERE r.disponible like '0'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function getInscriptosRosario(){
		$sql = "SELECT r.fechaHora,r.nombreCompleto,r.jornada
				FROM rosario r";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function procesarInscripcionRosario($datosInscripcion){
		$sql = "INSERT INTO rosario(fechaHora,nombreCompleto,jornada,disponible) VALUES(?,?,?,?)";
		$query = $this->db->query($sql,array($datosInscripcion['fechaHora'],$datosInscripcion['nombreCompleto'],$datosInscripcion['jornada'],$datosInscripcion['disponible']));
		if(!empty($query))
			return true;
		else
			return false;
	}

	public function actualizarDisponibilidad($datosInscripcion){
		$sql = "UPDATE rosario SET disponible = '0' WHERE(fechaHora = ?)";
		$query = $this->db->query($sql,array($datosInscripcion['fechaHora']));
	}

	public function obtenerNumFichaMedica(){
		$sql = "SELECT MAX(idFichaMedica) ultimoId
				FROM fichamedica fm";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function obtenerNumFichaPadrino(){
		$sql = "SELECT MAX(fp.idFichaPadrino) ultimoId
				FROM fichapadrino fp";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function obtenerNumFichaAhijado(){
		$sql = "SELECT MAX(fp.idFichaAhijado) ultimoId
				FROM fichaahijado fp";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
}
