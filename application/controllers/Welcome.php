<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('parser');
		$this->load->model('index','index');
	}

	public function index(){
		$datos= array(
			'menu' => $this->index->getMenu(),
			'comisiones' => $this->index->getComisiones(),
			'fechas' => $this->index->getFechas(),
			//'primerSetJornadas' => $this->index->getImagenesPrimerSetJornadas(),
			//'segundoSetJornadas' => $this->index->getImagenesSegundoSetJornadas(),
			//'tercerSetJornadas' => $this->index->getImagenesTercerSetJornadas(),
			'testimonios' => $this->index->getTestimonios(),
		);
		$this->parser->parse('index',$datos);
	}

	public function suscripcion(){
		if(isset($_POST['email']) && !empty($_POST['email'])){
			$email = filter_var ( $_POST['email'], FILTER_SANITIZE_EMAIL);
			$from = "contacto@mjvcmoron.com.ar";
			$to = "secretariado@mjvcmoron.com.ar";
			$subject = "Suscripcion a novedades";
			$message = "Direccion de correo a añadir a la lista de notificaciones: ".$email;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: '. $from . "\r\n";
			if(mail($to,$subject,$message, $headers)){
				echo 'OK';
			}else{
				echo 'Fail';
			}
		}

	}

	public function fichaMedica(){
		if(!empty($_POST)){
			if($_POST['alergia'] == 'Si'){
				$nombreAlergia = !empty($_POST['nombreAlergia']) ? filter_var($_POST['nombreAlergia'], FILTER_SANITIZE_STRING) : 'No completado';
			}else{
				$nombreAlergia = 'No posee alergias';
			}
			if($_POST['tratamiento'] == 'Si'){
				$medicamentoPorEnfermedad = !empty($_POST['medicamentoPorEnfermedad']) ? filter_var($_POST['medicamentoPorEnfermedad'], FILTER_SANITIZE_STRING) : 'No completado';
				$medicamentoASuministrar = !empty($_POST['medicamentoASuministrar']) ? filter_var($_POST['medicamentoASuministrar'], FILTER_SANITIZE_STRING) : 'No completado';
				$dosisMedicamentoASuministrar = !empty($_POST['dosisMedicamentoASuministrar']) ? filter_var($_POST['dosisMedicamentoASuministrar'], FILTER_SANITIZE_STRING) : 'No completado';
			}else{
				$medicamentoPorEnfermedad = 'No toma medicamento por enfermedad';
				$medicamentoASuministrar = 'Ningun medicamento a suministrar';
				$dosisMedicamentoASuministrar = 'Ninguna dosis requerida';
			}
			if($_POST['ocasiones'] == 'Si'){
				$nombreMedicamentoOcasional = !empty($_POST['nombreMedicamentoOcasional']) ? filter_var($_POST['nombreMedicamentoOcasional'], FILTER_SANITIZE_STRING) : 'No completado';
			}else{
				$nombreMedicamentoOcasional = 'No toma medicamento en forma ocasional';
			}
			if($_POST['medicamentoProhibido'] == 'Si'){
				$nombreMedicamentoProhibido = !empty($_POST['nombreMedicamentoProhibido']) ? filter_var($_POST['nombreMedicamentoProhibido'], FILTER_SANITIZE_STRING) : 'No completado';
			}else{
				$nombreMedicamentoProhibido = 'No tiene medicamento prohibido';
			}
			$datos = array(
				'nombreCompleto' => filter_var($_POST['nombreCompleto'], FILTER_SANITIZE_STRING),
				'grupoSanguineo' => filter_var($_POST['grupoSanguineo'], FILTER_SANITIZE_STRING),
				'telefono' => filter_var($_POST['telefono'], FILTER_SANITIZE_STRING),
				'email' => !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : 'No completado',
				'diabetes' => !empty($_POST['diabetes']) ? filter_var($_POST['diabetes'], FILTER_SANITIZE_STRING) : 'No completado',
				'alergias' => $_POST['alergia'],
				'tratamiento' => $_POST['tratamiento'],
				'ocasiones' => $_POST['ocasiones'],
				'medicamentoProhibido' => $_POST['medicamentoProhibido'],
				'problemaCoagulacion' => !empty($_POST['coagulacion']) ? filter_var($_POST['coagulacion'], FILTER_SANITIZE_STRING) : 'No completado',
				'celiaco' => !empty($_POST['celiaquia']) ? filter_var($_POST['celiaquia'], FILTER_SANITIZE_STRING) : 'No completado',
				'asma' => !empty($_POST['asma']) ? filter_var($_POST['asma'], FILTER_SANITIZE_STRING) : 'No completado',
				'cardiopatia' => !empty($_POST['cardiopatia']) ? filter_var($_POST['cardiopatia'], FILTER_SANITIZE_STRING) : 'No completado',
				'hipertension' => !empty($_POST['hipertension']) ? filter_var($_POST['hipertension'], FILTER_SANITIZE_STRING) : 'No completado',
				'nombreAlergia' => $nombreAlergia,
				'otraEnfermedad' => !empty($_POST['otraEnfermedad']) ? filter_var($_POST['otraEnfermedad'], FILTER_SANITIZE_STRING) : 'No completado',

				'medicamentoPorEnfermedad' => $medicamentoPorEnfermedad,
				'medicamentoASuministrar' => $medicamentoASuministrar,
				'dosisMedicamentoASuministrar' => $dosisMedicamentoASuministrar,
				'nombreMedicamentoOcasional' => $nombreMedicamentoOcasional,
				'nombreMedicamentoProhibido' => $nombreMedicamentoProhibido,
			);

			if($this->index->procesarFichaMedica($datos)){
				$numInscripto = $this->index->obtenerNumFichaMedica()[0]['ultimoId'] + 1;
				$this->generarPdf($datos,$numInscripto);
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeEnvioCorrecto' => 'La ficha medica se ha procesado con éxito!',
				);
			}else{
				//mensaje de aviso de falla
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeFallaEnvio' => 'Ha ocurrido un inconveniente en el envio de la ficha. Le solicitamos que intente en breve.',
				);
			}
			$this->parser->parse('index',$datos);

		}else{
			$this->load->view('index');
		}
	}

	public function generarPdf($datosFichaMedica,$numInscripto){
		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->AddPage();
		/*$html = "<!DOCTYPE html><html><head><title>Ficha medica</title></head><body><p>{$datosFichaMedica['nombreCompleto']}</p></body></html>";*/

		$html = "
				<!DOCTYPE html>
				<html>
				<head>
				
				</head>
				<p style='text-align: right;'>MOVIMIENTO DE JORNADAS DE VIDA CRISTIANA</p>
				<p style='text-align: right;'>50 A&Ntilde;OS, JOVENES EN SALIDA ANUNCIANDO LA ALEGRIA DEL EVANGELIO</p>
				<p style='text-align: center;'> FICHA M&Eacute;DICA DEL JORNADISTA</p>
				<br/><br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre y Apellido: </b> </label>
                            
                                <label> {$datosFichaMedica['nombreCompleto']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Grupo sangüineo: </b> </label>
                            
                                <label> {$datosFichaMedica['grupoSanguineo']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Email: </b> </label>
                            
                                <label> {$datosFichaMedica['email']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Telefono: </b> </label>
                            
                                <label> {$datosFichaMedica['telefono']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Daibetes: </b> </label>
                            
                                <label> {$datosFichaMedica['diabetes']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Alergias: </b> </label>
                            
                                <label> {$datosFichaMedica['alergias']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre de alergia: </b> </label>
                            
                                <label> {$datosFichaMedica['nombreAlergia']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Problemas de coagulación: </b> </label>
                            
                                <label> {$datosFichaMedica['problemaCoagulacion']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Celiaquía: </b> </label>
                            
                                <label> {$datosFichaMedica['celiaco']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Problemas de asma:</b> </label>
                            
                                <label> {$datosFichaMedica['asma']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Cardiopatías: </b> </label>
                            
                                <label> {$datosFichaMedica['cardiopatia']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Hipertensión: </b> </label>
                            
                                <label> {$datosFichaMedica['hipertension']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Otra enfermedad: </b> </label>
                            
                                <label> {$datosFichaMedica['otraEnfermedad']}</label>
                            
                </div>
                <br/>

                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Medicamento por enfermedad: </b> </label>
                            
                                <label> {$datosFichaMedica['medicamentoPorEnfermedad']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Medicamento a suministrar: </b> </label>
                            
                                <label> {$datosFichaMedica['medicamentoASuministrar']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Dosis medicamento a suministrar: </b> </label>
                            
                                <label> {$datosFichaMedica['dosisMedicamentoASuministrar']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Medicamento adicional: </b> </label>
                            
                                <label> {$datosFichaMedica['ocasiones']}</label>
                            
                </div>
                <br/>

                <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre medicamento adicional: </b> </label>
                            
                                <label> {$datosFichaMedica['nombreMedicamentoOcasional']}</label>
                            
                </div>
                <br/>

                 <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Medicamento que no puede tomar: </b> </label>
                            
                                <label> {$datosFichaMedica['medicamentoProhibido']}</label>
                            
                </div>
                <br/>

                 <br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre medicamento que no puede tomar: </b> </label>
                            
                                <label> {$datosFichaMedica['nombreMedicamentoProhibido']}</label>
                            
                </div>
                <br/>
                </body>
                </html>";
		//echo $html;
		$pdf->writeHTML($html, $ln = true, $fill = false, $reseth = false, $cell = false, $align = '');
		$nombre_archivo = 'Ficha medica de '.$datosFichaMedica['nombreCompleto'].'_'.$numInscripto.'.pdf';
		$pdfCreado = $pdf->Output(PDFPATH . $nombre_archivo, 'F');

	}

	public function fichaPadrino(){
		if(!empty($_POST)){

			$datos = array(
				'nombreCompleto' => filter_var($_POST['nombreCompleto'], FILTER_SANITIZE_STRING),
				'domicilio' => filter_var($_POST['domicilio'], FILTER_SANITIZE_STRING),
				'localidad' => filter_var($_POST['localidad'], FILTER_SANITIZE_STRING),
				'email' => !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : 'No completado',
				'telefono' => $_POST['telefono'],
				'celular' => $_POST['celular'],
				'edad' => $_POST['edad'],
				'jornada' => $_POST['jornada'],
				'ahijado' => filter_var($_POST['ahijado'], FILTER_SANITIZE_STRING),
				'conocimientoDelAhijado' => filter_var($_POST['conocimientoDelAhijado'], FILTER_SANITIZE_STRING),
				'relacionConJornadas' => filter_var($_POST['relacionConJornadas'], FILTER_SANITIZE_STRING),
				'motivoJornada' => filter_var($_POST['motivoJornada'], FILTER_SANITIZE_STRING),
				'ambienteSocial' => filter_var($_POST['ambienteSocial'], FILTER_SANITIZE_STRING),
				'defectosYVirtudes' => filter_var($_POST['defectosYVirtudes'], FILTER_SANITIZE_STRING),
				'vidaCristiana' => filter_var($_POST['vidaCristiana'], FILTER_SANITIZE_STRING),
				'hechosSignificativos' => filter_var($_POST['hechosSignificativos'], FILTER_SANITIZE_STRING),
			);

			if($this->index->procesarFichaPadrino($datos)){
				$numInscripto = $this->index->obtenerNumFichaPadrino()[0]['ultimoId'] + 1;
				$this->generarPdfFichaPadrino($datos,$numInscripto);
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeEnvioCorrecto' => 'La ficha del padrino se ha procesado con éxito!',
				);
			}else{
				//mensaje de aviso de falla
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeFallaEnvio' => 'Ha ocurrido un inconveniente en el envio de la ficha. Le solicitamos que intente en breve.',
				);
			}
			$this->parser->parse('index',$datos);

		}else{
			$this->load->view('index');
		}
	}

	public function generarPdfFichaPadrino($datos,$numInscripto){

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->AddPage();

		$html = "
				<!DOCTYPE html>
				<html>
				<head>
				
				</head>
				<p style='text-align: right;'>MOVIMIENTO DE JORNADAS DE VIDA CRISTIANA</p>
				<p style='text-align: right;'>50 A&Ntilde;OS, JOVENES EN SALIDA ANUNCIANDO LA ALEGRIA DEL EVANGELIO</p>
				<p style='text-align: center;'> FICHA DEL PADRINO</p>
				<br/><br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre y Apellido: </b> </label>
                            
                                <label> {$datos['nombreCompleto']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Telefono' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Telefono: </b> </label>
                            
                                <label> {$datos['telefono']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Domicilio: </b> </label>
                            
                                <label> {$datos['domicilio']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Celular: </b> </label>
                            
                                <label> {$datos['celular']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Email: </b> </label>
                            
                                <label> {$datos['email']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Localidad: </b> </label>
                            
                                <label> {$datos['localidad']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Edad: </b> </label>
                            
                                <label> {$datos['edad']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Jornada: </b> </label>
                            
                                <label> {$datos['jornada']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Ahijado: </b> </label>
                            
                                <label> {$datos['ahijado']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Conocimiento del ahijado: </b> </label>
                            
                                <label> {$datos['conocimientoDelAhijado']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Relación con Jornadas: </b> </label>
                            
                                <label> {$datos['relacionConJornadas']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Motivo de la Jornada: </b> </label>
                            
                                <label> {$datos['motivoJornada']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Ambiente social: </b> </label>
                            
                                <label> {$datos['ambienteSocial']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Defectos y virtudes: </b> </label>
                            
                                <label> {$datos['defectosYVirtudes']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Vida cristiana: </b> </label>
                            
                                <label> {$datos['vidaCristiana']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Hechos significativos: </b> </label>
                            
                                <label> {$datos['hechosSignificativos']}</label>
                            
                </div>
                <br/>
                <br/>";

		$pdf->writeHTML($html, $ln = true, $fill = false, $reseth = false, $cell = false, $align = '');
		$nombre_archivo = 'Ficha padrino de '.$datos['nombreCompleto'].'_'.$numInscripto.'.pdf';
		$pdfCreado = $pdf->Output(PDFPATH . $nombre_archivo, 'F');

	}

	public function fichaAhijado(){
		if(!empty($_POST)){

			$datos = array(
				'nombreCompleto' => filter_var($_POST['nombreCompleto'], FILTER_SANITIZE_STRING),
				'fechaNacimiento' => filter_var($_POST['fechaNacimiento'], FILTER_SANITIZE_STRING),
				'domicilio' => filter_var($_POST['domicilio'], FILTER_SANITIZE_STRING),
				'localidad' => filter_var($_POST['localidad'], FILTER_SANITIZE_STRING),
				'email' => !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : 'No completado',
				'telefono' => $_POST['telefonoFijo'],
				'celular' => $_POST['celular'],
				'estudios' => filter_var($_POST['estudiosCursados'], FILTER_SANITIZE_STRING),
				'titulo' => filter_var($_POST['titulo'], FILTER_SANITIZE_STRING),
				'trabajo' => filter_var($_POST['trabajo'], FILTER_SANITIZE_STRING),
				'ubicacion' => $_POST['ubicacionTrabajo'],
				'estadoCivil' => filter_var($_POST['estadoCivil'], FILTER_SANITIZE_STRING),
				'hijos' => $_POST['hijos'],
				'cantidadHijos' => $_POST['cantidadHijos'],
				'edad' => $_POST['edad'],
				'nombreCompletoPadrino' => filter_var($_POST['nombreCompletoPadrino'], FILTER_SANITIZE_STRING),
				'hechosSignificativos' => filter_var($_POST['hechosSignificativos'], FILTER_SANITIZE_STRING),
				'ideaDeDios' => filter_var($_POST['ideaDeDios'], FILTER_SANITIZE_STRING),
				'sacramentos' => filter_var($_POST['sacramentos'], FILTER_SANITIZE_STRING),
				'relacionConSacerdotes' => filter_var($_POST['relacionConSacerdotes'], FILTER_SANITIZE_STRING),
				'trabajosApostolicos' => filter_var($_POST['trabajosApostolicos'], FILTER_SANITIZE_STRING),
				'ambienteSocial' => filter_var($_POST['ambienteSocial'], FILTER_SANITIZE_STRING),
				'defectosYVirtudes' => filter_var($_POST['defectosYVirtudes'], FILTER_SANITIZE_STRING),
				'motivoJornada' => filter_var($_POST['motivoJornada'], FILTER_SANITIZE_STRING),
			);

			if($this->index->procesarFichaAhijado($datos)){
				$numInscripto = $this->index->obtenerNumFichaAhijado()[0]['ultimoId'] + 1;
				$this->generarPdfFichaAhijado($datos,$numInscripto);
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeEnvioCorrecto' => 'La ficha del ahijado se ha procesado con éxito!',
				);
			}else{
				//mensaje de aviso de falla
				$datos = array(
					'menu' => $this->index->getMenu(),
					'comisiones' => $this->index->getComisiones(),
					'fechas' => $this->index->getFechas(),
					'mensajeFallaEnvio' => 'Ha ocurrido un inconveniente en el envio de la ficha. Le solicitamos que intente en breve.',
				);
			}
			$this->parser->parse('index',$datos);

		}else{
			$this->load->view('index');
		}
	}

	public function generarPdfFichaAhijado($datos,$numInscripto){

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->AddPage();

		$html = "
				<!DOCTYPE html>
				<html>
				<head>
				
				</head>
				<p style='text-align: right;'>MOVIMIENTO DE JORNADAS DE VIDA CRISTIANA</p>
				<p style='text-align: right;'>50 A&Ntilde;OS, JOVENES EN SALIDA ANUNCIANDO LA ALEGRIA DEL EVANGELIO</p>
				<p style='text-align: center;'> FICHA DEL PADRINO</p>
				<br/><br/>
				<div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Nombre y Apellido: </b> </label>
                            
                                <label> {$datos['nombreCompleto']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Fecha nacimiento: </b> </label>
                            
                                <label> {$datos['fechaNacimiento']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Domicilio: </b> </label>
                            
                                <label> {$datos['domicilio']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Localidad: </b> </label>
                            
                                <label> {$datos['localidad']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Email: </b> </label>
                            
                                <label> {$datos['email']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Telefono' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Celular: </b> </label>
                            
                                <label> {$datos['celular']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Estudios: </b> </label>
                            
                                <label> {$datos['estudios']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Titulo: </b> </label>
                            
                                <label> {$datos['titulo']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Trabajo: </b> </label>
                            
                                <label> {$datos['trabajo']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Ubicación: </b> </label>
                            
                                <label> {$datos['ubicacion']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Estado civil: </b> </label>
                            
                                <label> {$datos['estadoCivil']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Hijos: </b> </label>
                            
                                <label> {$datos['hijos']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Cantidad de hijos: </b> </label>
                            
                                <label> {$datos['cantidadHijos']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Edad: </b> </label>
                            
                                <label> {$datos['edad']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Hechos significativos: </b> </label>
                            
                                <label> {$datos['hechosSignificativos']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Idea de Dios: </b> </label>
                            
                                <label> {$datos['ideaDeDios']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Sacramentos: </b> </label>
                            
                                <label> {$datos['sacramentos']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Relación con sacerdotes: </b> </label>
                            
                                <label> {$datos['relacionConSacerdotes']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Trabajos apostólicos: </b> </label>
                            
                                <label> {$datos['trabajosApostolicos']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Ambiente social: </b> </label>
                            
                                <label> {$datos['ambienteSocial']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Defectos y virtudes: </b> </label>
                            
                                <label> {$datos['defectosYVirtudes']}</label>
                            
                </div>
                <br/>
                <div class='form-group'>
                    <label for='Nombre y Apellido' class='col-lg-3 control-label'><i class='glyphicon glyphicon-flag'></i>&nbsp;&nbsp;<b>Motivo jornada: </b> </label>
                            
                                <label> {$datos['motivoJornada']}</label>
                            
                </div>
                <br/>
                <br/>";

		$pdf->writeHTML($html, $ln = true, $fill = false, $reseth = false, $cell = false, $align = '');
		$nombre_archivo = 'Ficha ahijado de '.$datos['nombreCompleto'].'_'.$numInscripto.'.pdf';
		$pdfCreado = $pdf->Output(PDFPATH . $nombre_archivo, 'F');

	}
}
