<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>MJVC Moron</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href=<?php echo "'" . base_url()?>css/bootstrap.css<?php echo "'"?> />
		<script type="text/javascript" href=<?php echo "'" . base_url()?>js/bootstrap.js<?php echo "'"?>></script>
		<script type="text/javascript" href=<?php echo "'" . base_url()?>js/jquery.js<?php echo "'"?>></script>
		<link rel="stylesheet" href=<?php echo "'" . base_url()?>assets/css/main.css<?php echo "'"?> />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		
		
		<noscript><link rel="stylesheet" href=<?php echo "'" . base_url()?>assets/css/noscript.css<?php echo "'"?> /></noscript>
		<style type="text/css">
			#main article
			{
				width: 100%;
			}
			.logo img
			{
				max-width: 90%;
				transform: translate(-50%, -50%);
				top: 50%;
				left: 50%;
				position: absolute;
			}
			input[type="text"]:disabled,input[type="radio"]:disabled + label::before {
			    background: grey;
			    border-color: grey;
			    cursor: not-allowed;
			}
		</style>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
		<div id="wrapper">
			<?php
                    if(isset($mensajeEnvioCorrecto)){
                        echo "<div class='alert alert-success' role='alert' id='successDiv'>
								  <button type='button' class='close' data-dismiss='alert' aria-label='Close' id='successButton'>
								    <span aria-hidden='true'>&times;</span>
								  </button>
									<h4><strong>{mensajeEnvioCorrecto}</strong></h4>
							</div>";
                    }elseif(isset($mensajeFallaEnvio)){
                        echo "<div class='alert alert-warning alert-dismissible fade show' role='alert' id='warningDiv'>
								  <button type='button' class='close' data-dismiss='alert' aria-label='Close' id='warningButton'>
								    <span aria-hidden='true'>&times;</span>
								  </button>
									<h4><strong>{mensajeFallaEnvio}</strong></h4>
							</div>";
                    }
            ?>

			<div class='alert alert-success' id='divExito' style='display:none;' role='alert'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><strong>La suscripci&oacute;n ha sido enviada correctamente.</strong></h4>
			</div>
			<div class='alert alert-warning' id='divAlerta' style='display:none;' role='alert'>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' id='successButton'> <span aria-hidden='true'>&times;</span> </button>
				<h4><strong>La suscripci&oacute;n no ha sido enviada. Intente nuevamente en unos minutos.</strong></h4>
			</div>

			<!-- Header -->
			<header id="header">
				<div class="logo">
					<img src=<?php echo "'" . base_url()?>images/logo_mjvc_reduced.png<?php echo "'"?>/>
					<!--<span class="icon fa-diamond"></span>-->
				</div>

				<nav>
					<ul>
						{menu}
						<li><a id="{idref}" href="{referencia}">{nombre}</a></li>
						{/menu}
					</ul>
				</nav>
				<!--<nav>
					<ul>
						<li><a href="#fichaPadrino">FICHA PADRINO</a></li>
						<li><a href="#fichaDatos">FICHA DATOS AHIJADO</a></li>
						<li><a href="#fichaAhijado">FICHA MEDICA AHIJADO</a></li>
					</ul>
				</nav>-->
			</header>

			<div id="divSuscripcion" class="row h-100" style="display: block;">
				<div class="col-12 my-auto">
					<div class="masthead-content text-white py-5 py-md-0">
						<br/><br/>
						<div class="input-group input-group-newsletter">
							<input type="email" name="emailSuscripcion" id="emailSuscripcion" class="text-white" placeholder="Ingresa tu email para recibir novedades!" aria-label="Ingresa tu email..." aria-describedby="basic-addon">
							<div class="input-group-append">
								<button class="btn btn-secondary" id="suscripcion" type="button">Suscribirme!</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br/><br/>
			<!-- Main -->
			<div id="main" style="width: 85%;">

				<!-- Intro -->
				<article id="nosotros">
					<h2 class="major">El MJVC Moron</h2>
					<span class="image main"><img src=<?php echo "'" . base_url()?>images/catedral.jpg<?php echo "'"?> alt="" /></span>
					<h4> ¿Qu&eacute; es el Movimiento de Jornadas de Vida Cristiana? </h4>
					<p>Es un movimiento de IGLESIA y para la IGLESIA. En su finalidad, quiere insertar a los jóvenes en sus respectivas parroquias y siendo de la iglesia y diocesano, trabaja con el reconocimiento de Obispo. Nace y vive en la Iglesia.
					Además es un Movimiento de jóvenes: “jóvenes, ustedes son la iglesia, son mi esperanza…” Juan Pablo II. Son los jóvenes los que podrán devolverle al cristianismo su dimensión heroica, en una época de actitudes absurdas, cómodas y cobardes. </p>
					<br/>
					<h4>ESENCIA Y FINALIDAD DEL MJVC</h4>
					<p>El MJVC es CRISTOCENTRICO, esto significa que es Cristo quien está en el punto de partida y de llegada, de un movimiento circular que dé El desciende hasta el corazón de los hombres, para que todos juntos volvamos a EL y con El al Padre pero, por supuesto, siempre de la mano de Maria nuestra guía y modelo. Pretende llevar al joven a la VIVENCIA DE LA VIDA, (CRISTO) como verdaderos HOMBRES , MUJERES que asumiendo a Cristo como maestro, vayan por un camino de SANTIDAD a cumplir esa misión APOSTOLICA que anuncian los Evangelios. Que opten por Cristo integralmente. Esta opción lograra la unidad entre la vida y la fe. La GRACIA SANTIICANTE es y será siempre la regla de nuestra vida, la fuerza que pugna por transformar nuestra vida. La Gracia es la presencia inmediata de DIOS en nuestra vida, es la fuerza impulsadora.</p>
					<br/><hr/><br/>
					<h4>METODOLOGIA DEL MJVC</h4>
					<p>Se conduce al Joven a un triple encuentro: CONSIGO MISMO, CON DIOS Y CON LOS DEMAS.
					A través de dos medios: PROCLAMACION DE LA PALABRA Y TESTIMONIO.
					Con método la pedagogía propia: esto posibilita que se cree en el joven una disposición a la integración del grupo de la jornada y le ayude a estar abierto al cambio y al compromiso. Junto al método, la Oración es el medio principal que se confía para el buen éxito de la jornada. En esto se manifiesta una devoción fuerte al espíritu santo y una devoción Mariana. Justamente Maria es el ejemplo del Si a la entrega a la voluntad de Dios.</p>
					<br/><hr/><br/>
					<h4>¿COMO LLEGA EL MJVC A LA ARGENTINA?</h4>
					<p>Un poco de Historia... Nos remontamos hacia fines de la década del cuarenta, allí se inician los "Cursillos de Cristiandad" que nacen como fruto de trabajos y meditación, de formar una mentalidad y cuajar un núcleo de ideas, ajenos a cualquier improvisación. El primer cursillo nace en 1949, iniciado para jóvenes. Vistos los logros obtenidos, fue nutriéndose de adultos, quienes resultaron más perseverantes, el tiempo hizo que los cursillos se orientaran hacia los adultos y desde España se irradian sobre el mundo. Con la necesidad de conquistar jóvenes, surge en México las "Jornadas de Vida Cristiana", modalidad independiente de los cursillos de Cristiandad. En 1966 viajan a México los Padres AdanRecofsky y Adolfo Ruhl con la finalidad de conocer y empaparse del Movimiento de Cursillos de Cristiandad pero allí se encuentran con el Movimiento de Jornadas de Vida Cristiana para Jóvenes. En octubre de 1967 tuvo lugar la primera jornada para muchachos en Rafael Calzada, diócesis de Lomas de Zamora. Con el transcurso del tiempo, se van sumando otros sacerdotes, llevando a Rafael Calzada jóvenes de otros lugares y con el tiempo comenzaron a realizarse jornadas directamente en otras diócesis. Así el Movimiento se extendió, creando comunidades, enriqueció a parroquias, revitalizando grupos de otros movimientos o instituciones juveniles.</p>
				</article>


				<article id="comisiones">
					<h2 class="major">Comisiones</h2>
					<div id="comisiones" class="services jarallax">
						<div class="container"> 
							<div class="agileits-title w3title2">
								<h3>Comisiones</h3>
								<p>Comisiones que integran el MJVC Moron</p>
							</div> 
							<div class="w3-services-grids">
								{comisiones}
								<div class="col-md-3 col-xs-6 w3l-services-grid">
									<div class="w3ls-services-img">
										<i class="{icono}" aria-hidden="true"></i>
									</div>
									<div class="agileits-services-info">
										<h4>{nombre}</h4>
										<p>{descripcion}</p>
									</div>
								</div>
								{/comisiones}
								
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</article>

				<!-- About -->
				<article id="eventos">
					<h2 class="major">Eventos</h2>
					<!--<span class="image main"><img src="images/eventos2.jpg" alt="" /></span>-->
					<div id="eventos" class="team agileits">
						<div class="team-agileinfo">
							<div class="container">  
								
								<div class="team-row agileits-w3layouts">
									<div class="clearfix"> </div>
									
									{fechas}
									<div class="col-md-3 col-xs-6 team-grids">
										<div class="wpf-demo-4">   
												<img src=<?php echo "'" . base_url()?>{pathImgReduced}<?php echo "'"?> class="img-responsive" alt=" " />   		
										</div>
									</div>
									{/fechas}
									
								</div>
							</div>
						</div>
					</div>
				</article>

				<article id="testimonios">
					<h2 class="major">Testimonios</h2>
					<!--<span class="image main"><img src="images/eventos2.jpg" alt="" /></span>-->
					<div id="eventos" class="team agileits">
						<div class="team-agileinfo">
							<div class="container">  
								
								<div class="team-row agileits-w3layouts">
									<div class="clearfix"> </div>
									
									{testimonios}
									<div class="col-md-8 col-xs-8 team-grids">
										  
										<h4 style="text-decoration-color: #0000FF">{jornadista} -- {jornada}</h4>
										<p>{testimonio}</p>		
										
									</div>
									{/testimonios}
									
								</div>
							</div>
						</div>
					</div>
				</article>

				<!-- Ficha padrino -->
				<article id="fichaPadrino">
					<h2 class="major">Ficha padrino</h2>
					<form method="post" action=<?php echo "'".base_url()?>Welcome/fichaPadrino<?php echo "'"?>>
						<div class="fields">
							<div class="field half">
								<label for="name">Nombre y apellido</label>
								<input type="text" name="nombreCompleto" id="nombreCompleto" required/>
							</div>
							<div class="field half">
								<label for="name">Domicilio</label>
								<input type="text" name="domicilio" id="domicilio" required/>
							</div>
							<div class="field half">
								<label for="name">Localidad</label>
								<input type="text" name="localidad" id="localidad" required/>
							</div>
							<div class="field half">
								<label for="email">Email</label>
								<input type="email" name="email" id="email" required/>
							</div>
							<div class="field half">
								<label for="name">Telefono</label>
								<input type="text" name="telefono" id="telefono" required/>
							</div>
							<div class="field half">
								<label for="name">Celular</label>
								<input type="text" name="celular" id="celular" required/>
							</div>
							<div class="field half">
								<label for="name">Edad</label>
								<input type="text" name="edad" id="edad" required/>
							</div>
							<div class="field half">
								<label for="name">Jornada</label>
								<input type="text" name="jornada" id="jornada" required/>
							</div>
							<div class="field half">
								<label for="name">Ahijado</label>
								<input type="text" name="ahijado" id="ahijado" required/>
							</div>
							<div class="field">                                     
								<h3 class="text-primary">Algunas preguntas</h3>
							</div> 
							<div class="field">
								<label>Explica de donde lo/a conoces, hace cuanto tiempo, ambiente que compartes con el / ella, que relación te une</label>
								<textarea name="conocimientoDelAhijado" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Qué relación tiene con la parroquia y con las jornadas?</label>
								<textarea name="relacionConJornadas" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Por qué consideras que debe hacer Jornadas?</label>
								<textarea name="motivoJornada" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Comenta cómo es su ambiente social, familiar y laboral?</label>
								<textarea name="ambienteSocial" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>Defectos y virtudes</label>
								<textarea name="defectosYVirtudes" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Cómo es su vida cristiana? (sacramentos, misa, apostolado)</label>
								<textarea name="vidaCristiana" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>Relata hechos significativos de su vida personal (niñez, adolescencia, juventud, problemas de salud, alimentación especial)</label>
								<textarea name="hechosSignificativos" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<ul class="actions">
									<li><input type="submit" value="Enviar" class="primary" /></li>
									<li><input type="reset" value="Reestablecer campos" /></li>
								</ul>
							</div>
						</div>
					</form>
				</article>

				<!-- Ficha ahijado -->
				<article id="fichaAhijado">
					<h2 class="major">Ficha Ahijado</h2>
					<form method="POST" action=<?php echo "'".base_url()?>Welcome/fichaAhijado<?php echo "'"?>>
						<div class="fields">

							<div class="field half">
								<label for="name">Nombre y apellido</label>
								<input type="text" name="nombreCompleto" id="nombreCompleto" required/>
							</div>
							<div class="field half">
								<label for="name">Fecha nacimiento</label>
								<input type="text" name="fechaNacimiento" id="fechaNacimiento" required/>
							</div>
							<div class="field half">
								<label for="name">Edad</label>
								<input type="text" name="edad" id="edad" required/>
							</div>
							<div class="field half">
								<label for="name">Domicilio</label>
								<input type="text" name="domicilio" id="domicilio" required/>
							</div>
							<div class="field half">
								<label for="name">Localidad</label>
								<input type="text" name="localidad" id="localidad" required/>
							</div>
							
							<div class="field half">
								<label for="name">Telefono</label>
								<input type="text" name="telefonoFijo" id="telefono" required/>
							</div>
							<div class="field half">
								<label for="name">Celular</label>
								<input type="text" name="celular" id="celular" required/>
							</div>
							<div class="field half">
								<label for="email">Email</label>
								<input type="email" name="email" id="email" />
							</div>
							<div class="field half">
								<label for="name">Estudios cursados</label>
								<input type="text" name="estudiosCursados" id="estudiosCursados" required/>
							</div>
							<div class="field half">
								<label for="name">Titulo</label>
								<input type="text" name="titulo" id="titulo" required/>
							</div>
							<div class="field half">
								<label for="name">Trabajo</label>
								<input type="text" name="trabajo" id="trabajo" required/>
							</div>
							<div class="field half">
								<label for="name">Ubicaci&oacute;n del trabajo</label>
								<input type="text" name="ubicacionTrabajo" id="ubicacionDelTrabajo" required/>
							</div>
							<div class="field half">
								<label for="name">Estado civil</label>
								<input type="text" name="estadoCivil" id="estadoCivil" required/>
							</div>
							<div class="field half">
								<label for="name">Hijos</label>
								<input type="text" name="hijos" id="hijos" required/>
							</div>
							<div class="field half">
								<label for="name">Cantidad de hijos</label>
								<input type="text" name="cantidadHijos" id="cantidadHijos" required/>
							</div>
							<div class="field half">
								<label for="name">Edades</label>
								<input type="text" name="edad" id="edad" required/>
							</div>

							<div class="field half">
								<label for="name">Nombre completo padrino/madrina</label>
								<input type="text" name="nombreCompletoPadrino" id="nombreCompletoPadrino" required />
							</div>
							<div class="field">
								<label>Hechos significativos en tu vida</label>
								<textarea name="hechosSignificativos" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Cu&aacute;l es tu idea acerca de Dios?</label>
								<textarea name="ideaDeDios" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Que Sacramentos tomaste?</label>
								<textarea name="sacramentos" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Que relaci&oacute;n con sacerdotes tienes?</label>
								<textarea name="relacionConSacerdotes" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Has realizado alg&uacute;n trabajo apost&oacute;lico? ¿Cuales?</label>
								<textarea name="trabajosApostolicos" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Cual es tu ambiente social de preferencia?</label>
								<textarea name="ambienteSocial" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>Describe cuales consideras que son tus defectos y virtudes</label>
								<textarea name="defectosYVirtudes" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label>¿Que te motiva a hacer jornada?</label>
								<textarea name="motivoJornada" class="form-control" rows="5" cols="20" required></textarea>
							</div>
							<div class="field">
								<label class="control-label text-Second">IMPORTANTE!!! No olvides tu medicación, ya que NO estamos autorizados a proporcionarla, por ello NECESITAMOS DE TU RESPONSABILIDAD!!!</label>
							</div>
						</div>
						<div class="field">
							<ul class="actions">
								<li><input type="submit" value="Enviar" class="primary" /></li>
								<li><input type="reset" value="Reestablecer campos" /></li>
							</ul>
						</div>
					</form>
				</article>

				<!-- Ficha datos -->
				<article id="fichaDatos">
					<h2 class="major">Ficha Medica</h2>
					<form method="POST" action=<?php echo "'".base_url()?>Welcome/fichaMedica<?php echo "'"?>>
						<div class="fields">

							<div class="field half">
								<label for="name">Nombre y apellido</label>
								<input type="text" name="nombreCompleto" id="nombreCompleto" required/>
							</div>
							<div class="field half">
								<label for="name">Grupo sangu&iacute;neo</label>
								<input type="text" name="grupoSanguineo" id="grupoSanguineo" required/>
							</div>
							<div class="field half">
								<label for="email">Email</label>
								<input type="email" name="email" id="email" />
							</div>
							<div class="field half">
								<label for="name">Telefono</label>
								<input type="text" name="telefono" id="telefono" required/>
							</div>
							<div class="field">                                   
								<h3 class="text-primary">Enfermedades crónicas en los últimos 3 años</h3>
							</div> 

							<div class="field half">
								<label>Diabetes</label>
								<div class="field half">
									<input type="radio" id="diabetesSi" value="Si" name="diabetes"/>
									<label for="diabetesSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="diabetesNo" value="No" name="diabetes" required/>
									<label for="diabetesNo">NO</label>
								</div>
							</div>

							<div class="field half">
								<label>Alergias</label>
								<div class="field half">
									<input type="radio" id="alergiaSi" value="Si" name="alergia">
									<label for="alergiaSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="alergiaNo" value="No" name="alergia" required>
									<label for="alergiaNo">NO</label>
								</div>
								<div class="field half" style="margin-top: 15px;">
									<label>¿A QUE? </label>
									<input type="text" name="nombreAlergia" class="form-control" id="idNombreAlergia" disabled>
								</div>  
							</div> 
							<div class="clear"></div>
							<div class="field half">
								<label>Problema de coagulación sanguínea</label>
								<div class="field half">
									<input type="radio" id="coagulacionSi" value="Si" name="coagulacion">
									<label for="coagulacionSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="coagulacionNo" value="No" name="coagulacion" required>
									<label for="coagulacionNo">NO</label>
								</div>
							</div>
							<div class="field half">
								<label>Celiaquía</label>
								<div class="field half">
									<input type="radio" id="celiaquiaSi" value="Si" name="celiaquia">
									<label for="celiaquiaSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="celiaquiaNo" value="No" name="celiaquia" required>
									<label for="celiaquiaNo">NO</label>
								</div>
							</div>
							<div class="field half">
								<label>Asma</label>
								<div class="field half">
									<input type="radio" id="asmaSi" value="Si" name="asma">
									<label for="asmaSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="asmaNo" value="No" name="asma" required>
									<label for="asmaNo">NO</label>
								</div>
							</div>
							<div class="field half">
								<label>Cardiopatías</label>
								<div class="field half">
									<input type="radio" id="cardiopatiaSi" value="Si" name="cardiopatia">
									<label for="cardiopatiaSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="cardiopatiaNo" value="No" name="cardiopatia" required>
									<label for="cardiopatiaNo">NO</label>
								</div>
							</div>
							<div class="field half">
								<label>Hipertensión</label>
								<div class="field half">
									<input type="radio" id="hipertensionSi" value="Si" name="hipertension">
									<label for="hipertensionSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="hipertensionNo" value="No" name="hipertension" required>
									<label for="hipertensionNo">NO</label>
								</div>
							</div>
							<div class="field half">
								<label>Alguna otra</label>
								<textarea name="otraEnfermedad" class="form-control" id="nombreApell"></textarea>
							</div>
							<div class="field">                                      
								<h3 class="text-primary">Medicamentos</h3>
							</div> 
							<div class="field">
								<label>¿Está actualmente en tratamiento con algún medicamento? </label>
								<div class="field half">
									<input type="radio" id="tratamientoSi" value="Si" name="tratamiento">
									<label for="tratamientoSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="tratamientoNo" value="No" name="tratamiento" required>
									<label for="tratamientoNo">NO</label>
								</div>
							</div>
							<div class="field">
								<label>En caso afirmativo complete lo siguiente:</label>
							</div>
							<div class="field half">
								<label>¿Por qué enfermedad lo toma? </label>
								<div class="col-lg-6">
									<input type="text" name="medicamentoPorEnfermedad" class="form-control medicamento" id="medicamentoPorEnfermedad" disabled required>
								</div>
							</div>
							<div class="field half">
								<label for="nombre" class="col-lg-6 control-label">¿Qué medicamento debe suministrarse? </label>
								<div class="col-lg-6">
									<input type="text" name="medicamentoASuministrar" class="form-control medicamento" id="medicamentoASuministrar" disabled required>
								</div>
							</div>

							<div class="field">
								<label for="nombre" class="col-lg-6 control-label">Dosis:  </label>
								<div class="col-lg-6">
									<input type="text" name="dosisMedicamentoASuministrar" class="form-control medicamento" id="dosisMedicamentoASuministrar" disabled required>
								</div>
							</div>
							<div class="field">
								<label>¿Toma algún medicamento en otras ocasiones?( fiebre, dolores, etc.)</label>
								<div class="field half">
									<input type="radio" id="medicamentoOcasionalSi" value="Si" class="medicamento" name="ocasiones" >
									<label for="medicamentoOcasionalSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="medicamentoOcasionalNo" value="No" class="medicamento" name="ocasiones" required>
									<label for="medicamentoOcasionalNo">NO</label>
								</div>
							</div>
							<div class="field">
								<label>¿Cuál?</label>
								<div class="field">
									<input type="text" name="nombreMedicamentoOcasional" class="form-control medicamento" id="nombreMedicamentoOcasional" disabled required>
								</div>
							</div>
							<div class="field">
								<label>¿ALGUN MEDICAMENTO QUE NO PUEDAS TOMAR?   </label>
								<div class="field half">
									<input type="radio" id="medicamentoProhibidoSi" class="medicamento" value="Si" name="medicamentoProhibido">
									<label for="medicamentoProhibidoSi">SI</label>
								</div>
								<div class="field half">
									<input type="radio" id="medicamentoProhibidoNo" class="medicamento" value="No" name="medicamentoProhibido" required>
									<label for="medicamentoProhibidoNo">NO</label>
								</div>
							</div>
							<div class="field">
								<label for="nombre" class="col-lg-6 control-label">¿Cuál?</label>
								<div class="field">
									<input type="text" name="nombreMedicamentoProhibido" class="form-control medicamento" id="nombreMedicamentoProhibido" disabled required>
								</div>
							</div>
							<div class="field">
								<label class="control-label text-Second">IMPORTANTE!!! No olvides tu medicación, ya que NO estamos autorizados a proporcionarla, por ello NECESITAMOS DE TU RESPONSABILIDAD!!!</label>
							</div>
						</div>
						<div class="field">
							<ul class="actions">
								<li><input type="submit" value="Enviar" class="primary" /></li>
								<li><input type="reset" value="Reestablecer campos" /></li>
							</ul>
						</div>
					</form>
				</article>

				<ul class="icons" style="margin-top: 50px;">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
				</ul>

				<!-- Footer -->
				<footer id="footer">
					<p class="copyright">&copy; MJVC Moron 2018.</p>
				</footer>
			</div>
		</div>

		<!-- BG -->
		<div id="bg"></div>

		<!-- Scripts -->
		<script src=<?php echo "'" . base_url()?>assets/js/jquery.min.js<?php echo "'"?>></script>
		<script src=<?php echo "'" . base_url()?>assets/js/browser.min.js<?php echo "'"?>></script>
		<script src=<?php echo "'" . base_url()?>assets/js/breakpoints.min.js<?php echo "'"?>></script>
		<script src=<?php echo "'" . base_url()?>assets/js/util.js<?php echo "'"?>></script>
		<script src=<?php echo "'" . base_url()?>assets/js/main.js<?php echo "'"?>></script>
		<script type="text/javascript">
			$('#tratamientoSi').click(function(event) {
				$('#medicamentoPorEnfermedad').removeAttr('disabled');
				$('#medicamentoASuministrar').removeAttr('disabled');
				$('#dosisMedicamentoASuministrar').removeAttr('disabled');
			});
			$('#tratamientoNo').click(function(event) {
				$('#medicamentoPorEnfermedad').attr('disabled', '');
				$('#medicamentoASuministrar').attr('disabled', '');
				$('#dosisMedicamentoASuministrar').attr('disabled', '');
			});

			$('#medicamentoOcasionalSi').click(function(event) {
				$('#nombreMedicamentoOcasional').removeAttr('disabled');
			});
			$('#medicamentoOcasionalNo').click(function(event) {
				$('#nombreMedicamentoOcasional').attr('disabled', '');
			});

			$('#medicamentoProhibidoSi').click(function(event) {
				$('#nombreMedicamentoProhibido').removeAttr('disabled');
			});
			$('#medicamentoProhibidoNo').click(function(event) {
				$('#nombreMedicamentoProhibido').attr('disabled', '');
			});

			$('#alergiaSi').click(function(event) {
				$('#idNombreAlergia').removeAttr('disabled');
			});
			$('#alergiaNo').click(function(event) {
				$('#idNombreAlergia').attr('disabled', '');
			});

			$('#successButton').click(function(event) {
				$('#divAlerta').css('display','none');
			});
			$('#warningButton').click(function(event) {
				$('#divExito').css('display','none');
			});
			$('#fichaPadrino').on('click',function()[{
				$('#divSuscripcion').css('display','none');
			});

			$('#suscripcion').click(function(){
				var data = $('#emailSuscripcion').val();
				$.post(<?php echo "'" . base_url()?>welcome/suscripcion<?php echo "'"?>,{email : data},function(response){
					if(response == 'OK'){
						$('#divAlerta').attr('style','display: none;');
						$('#divExito').attr('style','display: inline-block;');
					}else{
						$('#divExito').attr('style','display: none;');
						$('#divAlerta').attr('style','display: inline-block;');
					}

				}).fail(function(error) {
					console.log( error);
				});
			});

		</script>
	</body>
</html>
